# Dashboard QC
# By: Tristan Kaiser
# 11/3/2016

library(dplyr)
library(magrittr)
library(lubridate)
library(ggplot2)
library(scales)
# NOTES:
#

# 1. Adjust pull time-span with respect to Dashboard. Code refers to 'Launch' as beginning of the pull.
# 2. In this QC, 'Case Created Date' was used for the pull - this may cause problems with Resource Connections QC.
#    A feature (bug) that can be fixed in future.
# 3. Make sure to include all the following variables in the pull:
#    HL Client ID, Case ID, Resolution Category, Case Type, Case Date Enrolled, Case Date/Time Closed,
#    Need Date Closed, Impact Metric Category
# 4. Modify filepath accordingly
# 5. Change the Interval dates as instructed
# 6. Run this if lubridate is not working: Sys.setlocale("LC_TIME", "C")

# Load File (adjust accordingly)
df <- read.csv('C:/Users/KDesBois/Box Sync/Analytics/Data requests/! Customer Dashboards/2017/October/QC/Harlem_October.csv',
               header = TRUE, na.strings=c("", NA))

# Clean the data
names(df) <- gsub("...", ".", names(df), fixed = TRUE)
names(df) <- gsub("..", ".", names(df), fixed = TRUE)
df <- df %>%
  rename(`hl.client.id.phi` = `HL.Client.ID.Protected.Health.Information.`)
names(df) %<>% tolower
df <- df %>%
  mutate(`case.date.enrolled` = mdy_hm(`case.date.enrolled`),
         `case.date.time.closed` = mdy_hm(`case.date.time.closed`),
         `need.date.closed` = mdy_hm(`need.date.closed`))

# Intervals
#
# Note: Interval start dates are inclusive, end-dates are exclusive
#
# ENTER launch/dashboard/pull beginning date:
launch.date <- ymd(20160901)

# ENTER first day of Dashboard Month
dashboard.start <- ymd(20161001)

# ENTER last day of Dashboard Month (do not delete "+ days(1)")
dashboard.end <- ymd(20161031) + days(1)

# NO MODIFICATIONS BELOW THIS LINE -----------------------------------------------------------------------------------------------

# Launch interval
launch.interval <- interval(launch.date, dashboard.end)
# Dashboard interval
dashboard.interval <- interval(dashboard.start, dashboard.end)
# Last year interval
last.year.interval <- interval(dashboard.end - years(2), dashboard.end - years(1))
# This year interval
this.year.interval <- interval(dashboard.end - years(1), dashboard.end)
# Six month interval
six.month.interval <- interval(dashboard.end - months(6), dashboard.end)
# Total months in dashboard
total.months <- length(seq(from = launch.date, to = dashboard.end, by='month')) - 1

# UNIQUE PATIENTS -------------------------------------
#
# Unique Patients: Total
df.launch.unique.patients <- df %>%
  distinct(hl.client.id.phi, .keep_all = TRUE) %>%
  filter(case.type == 'Normal Client' & case.date.enrolled %within% launch.interval |
           case.type == 'RRR Client' & case.date.time.closed %within% launch.interval)
df.launch.unique.patients %>%
  count(case.type)
launch.unique.patients <- df.launch.unique.patients %>%
  summarise(n())
print(paste0("Unique Patients: Total: ", launch.unique.patients))

#Unique Patients: Dashboard Month
df.dashboard.unique.patients <- df.launch.unique.patients %>%
  filter((case.type == 'Normal Client' & case.date.enrolled %within% dashboard.interval) |
           (case.type == 'RRR Client' & case.date.time.closed %within% dashboard.interval))
df.dashboard.unique.patients %>%
  count(case.type)
dashboard.unique.patients <- df.dashboard.unique.patients %>%
  summarise(n())
print(paste0("Unique Patients: Dashboard Month: ", dashboard.unique.patients))

# Unique Patients: Average Month
avg.month.unique.patients <- launch.unique.patients/total.months
print(paste0("Unique Patients: Average Month: ", avg.month.unique.patients))

# Unique Patients: Total lives reached
launch.total.household.ratio <- df.launch.unique.patients %>%
  filter(is.na(number.in.household.total) == FALSE & number.in.household.total > 0) %>%
  summarise(mean(number.in.household.total))
launch.total.lives.reached <- launch.unique.patients * launch.total.household.ratio
print(paste0("Unique Patients: Total Lives Reached: ", launch.total.lives.reached))

# Benchmark
# Calculate elsewhere

# RESOURCE CONNECTIONS -------------------------------------
#
# Resource Connections made
#  We use df because we calculate at the need level

#Resource Connections: Total
#
# Successful Only
launch.successful.cxns <- df %>%
  filter(need.date.closed %within% launch.interval &
           resolution.category == 'Success') %>%
  summarise(n())
print(paste0("Resource Connections: Successful Total: ", launch.successful.cxns))
#
# Successful + Equipped
launch.successful.equipped.cxns <- df %>%
  filter(need.date.closed %within% launch.interval &
           (resolution.category == 'Success' |
           resolution.category == 'Equipped')) %>%
  summarise(n())
print(paste0("Resource Connections: Successful + Equipped Total: ", launch.successful.equipped.cxns))

# Resource Connections: Dashboard Month
#
# Successful Only
dashboard.successful.cxns <- df %>%
  filter(need.date.closed %within% dashboard.interval &
           resolution.category == 'Success') %>%
  summarise(n())
print(paste0("Resource Connections: Dashboard Month: Successful: ", dashboard.successful.cxns))
#
# Successful + Equipped
dashboard.successful.equipped.cxns <- df %>%
  filter(need.date.closed %within% dashboard.interval &
           (resolution.category == 'Success' |
              resolution.category == 'Equipped')) %>%
  summarise(n())
print(paste0("Resource Connections: Dashboard Month: Successful + Equipped: ", dashboard.successful.equipped.cxns))

# Resource Connections: Average Month 
#
# Successful Only
avg.successful.cxns <- launch.successful.cxns / total.months
print(paste0("Resource Connections: Average Month: Successful:", avg.successful.cxns))
#
#Successful + Equipped
avg.successful.equipped.cxns <- launch.successful.equipped.cxns / total.months
print(paste0("Resource Connections: Average Month: Successful + Equipped: ", avg.successful.equipped.cxns))

# % Successful / All Connections
launch.percent.successful <- launch.successful.cxns / launch.successful.equipped.cxns
print(paste0("Resource Connections: % Successful / All Connections: ", launch.percent.successful))

# Benchmark
# Calculate elsewhere

# SCREENING & REFERRAL IN DASHBOARD MONTH -------------------------------------
# Screening Tracker data needed

# Screening and Referral: Dashboard Month: % of Clinic Screened
#   Screening and Referral: Dashboard Month: (# screened / eligible patients)
# Screening and Referral: Average Month
# Screening and Referral: Dashboard Month: Positive Screen Rate
# Screening and Referral: Dashboard Month: Referring Providers
# Screening and Referral: Dashboard Month: Referrals / Provider
# Screening and Referral: Dashboard Month: Enrollment Rate
#   Screening and Referral: Dashboard Month: (# enrolled / positive screens)


# TOP PRESENTING NEEDS -------------------------------------
#
# RIGHT NOW THIS IS ONLY FOR CCRMC & KP
# # Closed
# Do we still need to exclude sub-need type 'other'?
df.launch.closed.needs <- df %>%
  filter(`resolution.category` != 'Declined HL Services',
         `resolution.category` != 'NA',
         case.type != 'Triage Only',
         need.date.closed %within% launch.interval)
launch.closed.needs <- df.launch.closed.needs %>%
  count(service.type.category) %>%
  arrange_(~ desc(n)) #%>%
 # head(n = 8)
print("Top Presenting Needs: # Closed: ")
print(launch.closed.needs)

# % Connected (For both Success and Equipped)
# Need to make just Successful
df.launch.connected.needs <- df.launch.closed.needs %>%
  filter(resolution.category == 'Success' |
        resolution.category == 'Equipped')
launch.connected.needs <- df.launch.connected.needs %>%
  count(service.type.category)
launch.percent.connected <- left_join(launch.closed.needs, launch.connected.needs, by = "service.type.category")
launch.percent.connected <- launch.percent.connected %>%
  mutate(percent.connected = round((n.y / n.x) * 100, 2))
print("Top Presenting Needs: % Connected: ")
print(launch.percent.connected)

# Days open (Average)
launch.days.open <- df.launch.closed.needs %>%
  group_by(service.type.category) %>%
  summarise("Days Open" = round(mean(days.open, na.rm = TRUE),0))
launch.days.open <- left_join(launch.percent.connected, launch.days.open, by = "service.type.category")
print("Top Presenting Needs: Days Open: ")
print(launch.days.open)

# RESULTS -------------------------------------
#
# Launch to Date Impact Metrics (Need to clean up)
launch.impact.metric <- df.launch.unique.patients %>%
  filter(need.date.closed %within% launch.interval,
         impact.metric.category != "N/A - No Needs Resolved",
         impact.metric.category != "RRR") %>% 
  count(impact.metric.category)
launch.impact.metric <- launch.impact.metric %>%
  mutate(percentage = round((n / sum(n)) * 100, 0))
print("Results: % of patients who...: Total: ")
print(launch.impact.metric)

# PATIENT EXPERIENCE -------------------------------------
#
# Contacts per patient
launch.avg.contacts <- df.launch.unique.patients %>%
  summarise(round(mean(x.successful.contacts),1))
print(paste0("Patient Experience: Contacts per patient: ", launch.avg.contacts))

# Avg. days of engagement
launch.avg.days.engagement <- df.launch.unique.patients %>%
  filter(case.type == 'Normal Client' & case.status == 'Closed') %>%
  summarise(round(mean(days.enrolled),0))
print(paste0("Patient Experience: Average Days of Engagement: ", launch.avg.days.engagement))

# NPS
this.year.avg.patient.nps <- df.launch.unique.patients %>%
  filter(case.date.enrolled %within% this.year.interval) %>%
  count(nps)
print(paste0("Patient Experience: Past 12 Months: NPS: ", this.year.avg.patient.nps))

# Clinic rating due to HL
this.year.avg.patient.clinic.rating <- df.launch.unique.patients %>%
  filter(case.date.enrolled %within% this.year.interval) %>%
  count(client.survey.clinic.rating)
print(paste0("Patient Experience: Past 12 Months: Average Clinic Rating: ", this.year.avg.patient.clinic.rating))

# ADVOCATES -------------------------------------
# AVS
#
# # of advocates
# NPS
# Patients per advocate
# # FTEs

# BAR CHART ---------------------------------------
# Where to get data for Screens Returned & Positive Screens?
# Enrollments
six.month.enrollments <- df.launch.unique.patients %>%
  filter(case.type == 'Normal Client' & case.date.enrolled %within% six.month.interval) %>%
  mutate(month = as.Date(cut(case.date.enrolled, breaks = "month")))

six.month.enrollments <- six.month.enrollments %>%
  group_by(month) %>%
  summarise("Total.Enrollments" = n())

ggplot(data = six.month.enrollments,
       aes(x = month, y = Total.Enrollments)) +
  geom_bar(stat = "identity") +
  geom_text(aes(label=Total.Enrollments), vjust= 3) +
  scale_x_date(
    labels = date_format("%Y-%m"),
    date_breaks = "1 month")

# Screens Returned
# Positive Screens

## PAGE 2 ## ------------------------------------------------------------------------

# YEAR-OVER-YEAR COMPARISON -------------------------------------

# Last Year Unique patients
df.last.year.unique.patients <- df %>%
  distinct(hl.client.id.phi, .keep_all = TRUE) %>%
  filter(case.type == 'Normal Client' & case.date.enrolled %within% last.year.interval |
           case.type == 'RRR Client' & case.date.time.closed %within% last.year.interval)
last.year.unique.patients <- df.last.year.unique.patients %>%
  summarise(n())
print(paste0("Year-Over-Year Comparison: Unique Patients: Last Year: ", last.year.unique.patients))

# This Year Unique patients
df.this.year.unique.patients <- df %>%
  distinct(hl.client.id.phi, .keep_all = TRUE) %>%
  filter(case.type == 'Normal Client' & case.date.enrolled %within% this.year.interval |
           case.type == 'RRR Client' & case.date.time.closed %within% this.year.interval)
this.year.unique.patients <- df.this.year.unique.patients %>%
  summarise(n())
print(paste0("Year-Over-Year Comparison: Unique Patients: This Year: ", this.year.unique.patients))

# Resource Connections
# Last Year
# Successful
last.year.successful.cxns <- df %>%
  filter(need.date.closed %within% last.year.interval &
           resolution.category == 'Success') %>%
  summarise(n())
print(paste0("Year-Over-Year Comparison: Resource Connections: Succesful: Last Year: ", last.year.successful.cxns))
#
# Equipped
last.year.equipped.cxns <- df %>%
  filter(need.date.closed %within% last.year.interval &
              resolution.category == 'Equipped') %>%
  summarise(n())
print(paste0("Year-Over-Year Comparison: Resource Connections: Equipped: Last Year: ", last.year.equipped.cxns))
#
# This Year
# Successful Only
this.year.successful.cxns <- df %>%
  filter(need.date.closed %within% this.year.interval &
           resolution.category == 'Success') %>%
  summarise(n())
print(paste0("Year-Over-Year Comparison: Resource Connections: Succesful: This Year: ", this.year.successful.cxns))
#
# Equipped
this.year.equipped.cxns <- df %>%
  filter(need.date.closed %within% this.year.interval &
              resolution.category == 'Equipped') %>%
  summarise(n())
print(paste0("Year-Over-Year Comparison: Resource Connections: Equipped: This Year: ", this.year.equipped.cxns))

# Screening and Referral in Dashboard Month
#
# % of clinic screened
# Get elsewhere

# Positive Screen Rate
# Get elsewhere

# Referrals per provider
# Get elsewhere

# % of patients who... (Impact Metric)
# Last Year Impact Metric
last.year.impact.metric <- df.last.year.unique.patients %>% 
  filter(need.date.closed %within% last.year.interval,
         impact.metric.category != "N/A - No Needs Resolved",
         impact.metric.category != "RRR") %>%
  count(impact.metric.category)
last.year.impact.metric  <- last.year.impact.metric  %>%
  mutate(percentage = round((n / sum(n) * 100),0))
print("Year-Over-Year Comparison: % of patients who...: Last Year: ")
print(last.year.impact.metric)
#
# This Year Impact Metric
this.year.impact.metric <- df.this.year.unique.patients %>% 
  filter(case.type != 'Triage Only' & need.date.closed %within% this.year.interval,
         impact.metric.category != "N/A - No Needs Resolved",
         impact.metric.category != "RRR") %>% 
  count(impact.metric.category)
this.year.impact.metric  <- this.year.impact.metric  %>%
  mutate(percentage = round((n / sum(n) * 100), 0))
print("Year-Over-Year Comparison: % of patients who...: This Year: ")
print(this.year.impact.metric)

# Patient Satisfaction (NPS)
last.year.avg.patient.nps <- df.launch.unique.patients %>%
  filter(case.date.enrolled %within% last.year.interval) %>%
  summarise(round(mean(nps, na.rm = TRUE),0))
print(paste0("Year-Over-Year Comparison: Patient Satisfaction: Last Year: ", last.year.avg.patient.nps))

this.year.avg.patient.nps <- df.launch.unique.patients %>%
  filter(case.date.enrolled %within% this.year.interval) %>%
  summarise(round(mean(nps, na.rm = TRUE),0))
print(paste0("Year-Over-Year Comparison: Patient Satisfaction: This Year: ", this.year.avg.patient.nps))

# Advocate Satisfaction (NPS)
# Data?

# KEY SUBNEEDS AND COMMUNITY RESOURCES -------------------------------

# Identify top closed need domains
launch.identify.top.closed.needs <- df.launch.closed.needs %>%
  filter(service.type.category != "Housing", 
         !grepl("^Unknown|^Other", service.type.sub.need)) %>%
  count(service.type.category) %>%
  arrange_(~ desc(n)) %>%
  slice(1:3)
df.launch.top.closed.needs <- semi_join(df.launch.closed.needs, launch.identify.top.closed.needs, by = "service.type.category")

# # Closed
launch.top.closed.subneeds <- df.launch.top.closed.needs %>%
  filter(!grepl("^Unknown|^Other", service.type.sub.need)) %>%
  group_by(service.type.category, service.type.sub.need) %>%
  count(service.type.sub.need) %>%
  arrange(service.type.category, desc(n)) %>%
  top_n(n=5)

# % Connected
launch.top.connected.subneeds <- df.launch.connected.needs %>%
  count(service.type.sub.need)
launch.top.percent.connected.subneeds <- left_join(launch.top.closed.subneeds, launch.top.connected.subneeds, by = "service.type.sub.need")
launch.top.percent.connected.subneeds <- launch.top.percent.connected.subneeds %>%
  mutate(percent.connected = round((n.y / n.x) * 100, 0))

# Days Open (Average)
launch.top.days.open.subneeds <- df.launch.closed.needs %>%
  group_by(service.type.sub.need) %>%
  summarise(days.open = round(mean(`days.open`, na.rm=TRUE), 0))
launch.top.days.open.subneeds <- left_join(launch.top.percent.connected.subneeds, launch.top.days.open.subneeds, by = "service.type.sub.need")

names(launch.top.days.open.subneeds)[names(launch.top.days.open.subneeds)=="n.x"] <- "n.closed"
launch.closed.connected.daysopen.subneeds <- launch.top.days.open.subneeds %>%
  select(- n.y)
print("Key Subneeds and Community Resources: # Closed, % Connected, and Days Open")
print(launch.closed.connected.daysopen.subneeds)

# Patient Demographics ---------------------------------------
launch.gender <- df.launch.unique.patients %>%
  filter(gender != 'Declined to State',
         gender != 'NA') %>%
  count(gender) %>%
  mutate(freq = round((n / sum(n))*100, 2))
print("Patient Demographics: Gender: ")
print(launch.gender)

launch.insurance.status <- df.launch.unique.patients %>%
  filter(insurance.status != 'NA') %>%
  count(insurance.status) %>%
  arrange_(~ desc(n)) %>%
 # head(n = 8) %>%
  mutate(freq = round((n / sum(n))*100, 0))
print("Patient Demographics: Insurance Status: ")
print(launch.insurance.status)

launch.top.zipcodes <- df.launch.unique.patients %>%
  count(zip.code.protected.health.information.) %>%
  arrange_(~ desc(n)) %>%
 # head(n = 8) %>%
  mutate(freq = round((n / sum(n)*100), 0))
print("Patient Demographics: Top ZIP codes")
print(launch.top.zipcodes)

# Affiliated Reources
# TBD